# NeurIPS Task 3 Solution

Copyright (C) 2020 - Jessica McBroom and Benjamin Paassen  
The University of Sydney

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.


The solution for task 3 of the [NeurIPS 2020 Education Challenge](https://eedi.com/projects/neurips-education-challenge) by the Quokka Appreciation Team.

## LICENSES
The code is licensed under GPL 3.